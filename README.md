# CUDA implementation of NTT multiplication

This is a group project for implementing CUDA accelerated code for NTT multiplication as part of course ECE-9413: Parallel computing.

## To run the cuda project:
### 1. glone this github repository on your working directory
### 2. run "make && ./ntt_cuda"
### 3. rum "make clean" to delete output files
